let Items = React.createClass({
  
    displayName: 'Item',


    propTypes: { 
      id: React.PropTypes.number,
      cond : React.PropTypes.bool,
      name: React.PropTypes.string,
      img : React.PropTypes.any,
      model : React.PropTypes.string,
      cost : React.PropTypes.string,
      count:  React.PropTypes.number ,
      cbfunHighlight:React.PropTypes.func,
      cbfunDelete : React.PropTypes.func, 
     
    },

    getInitialState: function (){
      return {keyItem : this.props.id,
              condition : this.props.cond,
              nameItem : this.props.name,
              imgItem : this.props.img,
              modelItem : this.props.model,
              costItem : this.props.cost,
              countItem : this.props.count,
             }
   },

   funHighlight: function (num){
      this.props.cbfunHighlight(num)
   },

   funDelete: function(arg){
      if(prompt("bla bla bla") != null){
          this.props.cbfunDelete(arg)
      }
   },

   render: function(){
       let viewClass;
       if (!this.state.condition){viewClass = "Item"}else{ viewClass = "Orange"}
    return  React.DOM.div({key:this.state.keyItem, className: viewClass,onClick:() => this.funHighlight(this.state.keyItem,this.state.nameItem,this.state.modelItem,this.state.imgItem,this.state.costItem, this.state.countItem)},
                        React.DOM.div({className:"divNameItem"},
                              React.DOM.p( null, this.state.nameItem ),
                              ),
                        React.DOM.div({className: "photo"},
                              React.DOM.img({className: "img", src :this.state.imgItem}), 
                              ),
                        React.DOM.div({className: "dataItem" },      
                              React.DOM.div({className:"divModelItem"},    
                                  React.DOM.p(null, this.state.modelItem),
                              ),
                              React.DOM.div({className: "divCost"},     
                                  React.DOM.p(null, this.state.costItem),
                              ),
                              React.DOM.div({className:"divCount"},
                                  React.DOM.button({onClick : () =>  this.funDelete(this.state.keyItem) } , "delete"),
                                  React.DOM.p(null, this.state.countItem),
                              ),
                         ),    
                      )
    },
 })


 /* React.DOM.div({key:keyItem, className: "Orange"},
                          React.DOM.div({className:"divNameItem"},
                                React.DOM.p( null, nameItem ),
                                ),
                          React.DOM.div({className: "photo"},
                                React.DOM.img({className: "img", src : imgItem}), 
                                ),
                          React.DOM.div({className: "dataItem" },      
                                React.DOM.div({className:"divModelItem"},    
                                    React.DOM.p(null, modelItem),
                                ),
                                React.DOM.div({className: "divCost"},     
                                    React.DOM.p(null, costItem),
                                ),
                                React.DOM.div({className:"divCount"},
                                    React.DOM.button({onClick : () =>  this.funDelete(item.id) } , "delete"),
                                    React.DOM.p(null, countItem),
                                ),
                          ),
                      ), */