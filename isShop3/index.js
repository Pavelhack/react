let IsShop = React.createClass({
  
    displayName: 'IsShop',

    getDefaultProps: function(){
      return { nathing: "Here is empty" }
    },

    propTypes: {
      title : React.PropTypes.string,
      item : React.PropTypes.array,
    },

    getInitialState: function (){
         
      return {view: 0,
              arr: this.props.item,
            }
    },

    funHighlight: function(num){
        this.setState({view : num})
       
    },

    funDelete: function(arg){
        filt  = this.state.arr.filter(item => item.id != arg)
        this.setState({arr: filt})
    },
 
   
    render: function(){
      let list, boolVar;
        list  = this.state.arr.map(item =>{
                                      if(this.state.view == item.id){boolVar = true}else{boolVar = false}
                                       return            React.createElement(View,{ key : item.id,
                                                                              id : item.id,
                                                                              cond : boolVar,
                                                                              name: item.name,
                                                                              img : item.img, 
                                                                              model : item.model,
                                                                              cost : item.cost,
                                                                              count: item.count, 
                                                                              cbfunHighlight: this.funHighlight, 
                                                                              cbfunDelete : this.funDelete
                                                                      }
                                              )
                                                                    })
      return React.DOM.div( {className:'IsShopFrame'},
                React.DOM.div({className:'header'},
                React.DOM.h1(null, this.props.title)),
                     React.DOM.div({className: 'ItemDiv'}, list),
                React.DOM.span(null, this.props.nathing),
              )
    },

});

 