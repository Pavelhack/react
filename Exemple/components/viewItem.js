import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import './index.css';

class ViewItem extends React.Component{

    static propTypes = { 
        keyItem : PropTypes.number,
        nameItem : PropTypes.string,
        modelItem : PropTypes.string,
        costItem : PropTypes.number,
        countItem : PropTypes.number
      };
  
     state ={
     };

 render(){
            return( 
                     <div className = "NewProd">
                            <div className = {"header"}>
                                <h1>{"Information about"+"  "+ this.props.nameItem}</h1>
                            </div>
                                <h3>{"ID:"+this.props.keyItem}</h3>
                                <h3>{"Model :"+" "+this.props.modelItem}</h3>
                                <h3>{"Cost :"+" "+this.props.costItem+"$"}</h3>
                                <h3>{"Count :"+" "+this.props.countItem}</h3>
                            </div>
                   )
           }
};
export default  ViewItem;    