import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import './index.css';

class Product extends React.Component{

    static propTypes = { 
        cbfunViewItem : PropTypes.func,
        keyItem : PropTypes.number,
        nameItem : PropTypes.string,
        modelItem : PropTypes.string,
        costItem : PropTypes.number,
        countItem : PropTypes.number,
        img: PropTypes.any,
      };
  
     state ={
     };

 render(){
            return( 
                    <div className = {"orange"} >
                        <div className = {"divNameItem"}>
                            <p>{this.props.nameItem}</p>
                            <p>{this.props.keyItem}</p>
                        </div>
                        <div className = {"photo"}>
                            <img className = {"img"} src = {this.props.img}/> 
                        </div>
                        <div className = {"dataItem"}>      
                                <div className = {"divModelItem"}>    
                                    <p>{this.props.modelItem}</p>
                                </div>
                                <div className = {"divCost"}>     
                                <p>{this.props.costItem + "$"}</p>
                                </div>
                                <div className = {"divCount"}>
                                    <button onClick = { () => this.funEdit(item.id)}>{"Edit"}</button>
                                    <button onClick = { () =>  this.funDelete(item.id) }>{"delete"}</button>
                                    <p>{this.props.countItem}</p>
                                </div>
                        </div>    
                    </div>
                   )
           }
};
export default  Product;    