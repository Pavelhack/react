import React from 'react';
import PropTypes, { bool } from "prop-types"
import './index.css';
import Product from './product.js';
class Item extends React.Component{

   static propTypes = { 
      prop : PropTypes.array,
      cbfunViewItem : PropTypes.func,
    };

   state ={
      id : 0,
      arr: this.props.prop,
   };

   funViewItem = (id,) => {
         this.setState({id : id}) 
         this.props.cbfunViewItem(id,)
   }
   

   render(){
      let ok = [],keyItem, nameItem, imgItem, modelItem, costItem, countItem, fun, prod;
      this.state.arr.map(item =>{
               keyItem = item.id,  nameItem = item.name, imgItem = item.img,
               modelItem = item.model, costItem = item.cost, countItem = item.count
               if(this.state.id == keyItem){ 
                 prod =   < Product key = {keyItem}  keyItem = {item.id} nameItem = {nameItem} img = {imgItem} modelItem = {modelItem} costItem = {costItem} countItem = {countItem} />
                 ok.push(prod)
                  }else{ 
             fun = <div key = {keyItem} className = {"Item"} onClick ={() => {this.funViewItem(item.id)}}>
                        <div className = {"divNameItem"}>
                              <p>{nameItem}</p> 
                              <p>{keyItem}</p>
                        </div>
                        <div className = {"photo"}>
                              <img className = {"img"} src = {imgItem}/> 
                        </div>
                          <div className = {"dataItem"}>      
                          <div className = {"divModelItem"}>    
                                    <p>{modelItem}</p>
                                </div>
                                <div className = {"divCost"}>     
                                    <p>{costItem + "$"}</p>
                                </div>
                                <div className = {"divCount"}>
                                      <button onClick = { () => this.funEdit(item.id)}>{"Edit"}</button>
                                      <button onClick = { () => this.funDelete(item.id) }>{"delete"}</button>
                                      <p>{countItem}</p>
                                </div>
                        </div>    
                      </div>
                     
                      ok.push(fun)
                  }

                    })
                  return   <div className = "ItemDiv">{ok}</div>
    }
 };

 export default  Item;