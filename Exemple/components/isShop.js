import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import './index.css';
import Item  from './items.js'; 
import Product from './product.js'
//import Edit from './edit.js';
import ViewItem from './viewItem.js';
//import Newfile from "./newFile.js";    
class IsShop extends React.Component{

  static propTypes  = {
    title : PropTypes.string,
    flag: PropTypes.bool,
    item : PropTypes.arrayOf(
       PropTypes.shape({
          count: PropTypes.number.isRequired,
          name: PropTypes.string.isRequired,
          cost: PropTypes.number.isRequired,
          img: PropTypes.any,
    })
    ),
  };

  static defaultProps = { 
    nathing: "Here is empty" 
  };

  state = {
    arr: this.props.item,
    viewId : 0,
   
  };

  funViewItem = (id) =>{
    this.setState({viewId : id})
  };


    render() {
      let keyItem, nameItem, imgItem, modelItem, costItem, countItem, back;
      this.state.arr.map(item =>{
               keyItem = item.id,  nameItem = item.name, imgItem = item.img,
               modelItem = item.model, costItem = item.cost, countItem = item.count
      
             if (this.state.viewId == 0){ 
     back = <Fragment>
            <div className ={"IsShopFrame"}>
              <div className = {"header"}>
                   <h1>{this.props.title}</h1>
              </div>
                  <Item prop = {this.state.arr} cbfunViewItem = {this.funViewItem} /> 
            </div>
            </Fragment>
            
    } else if(this.state.viewId  == keyItem) { 
      back = <Fragment>  
              <div className ={"IsShopFrame"}>
                <div className = {"header"}>
                    <h1>{this.props.title}</h1>
                </div>
                    <Item prop = {this.state.arr}   cbfunViewItem = {this.funViewItem}  />
              </div>
              <div>
                    <ViewItem  keyItem = {keyItem} nameItem = {nameItem} imgItem = {imgItem} modelItem = {modelItem} costItem = {costItem} countItem = {countItem} />
              </div>
            </Fragment>
           };
      })
      return back
   };
};

export default IsShop;