"use strict";

import React  from 'react';
import ReactDOM from 'react-dom';
import './components/index.css';
import IsShop from './components/isShop.js';

let flag = false;
let nameStor = "Store React";
let itemArr = require("./array.json");

    ReactDOM.render(
      
        <IsShop
          title = {nameStor}
          item = {itemArr}
          flag = {flag}
       
        />
        
     , document.getElementById('container') 
    );
