import React, { Fragment } from 'react';
import PropTypes, { bool } from "prop-types"
import './index.css';
import {connect} from 'react-redux'
import {setChangeItem, setStateEdit, setEdit } from './action/PageActons.js';

class Edit extends React.Component{

     state ={
       nameInput : "",
       nameDanger :  "Please, change the field. Value must be a string",
       costInput : "",
       costDanger : "Please, change the field. Value must be a number",
       countInput : "",
       countDanger : "Please, change the field. Value must be a number",
       modelName : "",
       modelDanger : "Please, change the field. Value must be a string",
     };

     changeInputName = (EO) =>{
          this.props.setStateEdit(false);
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({nameInput : EO.target.value , nameDanger:"OK"})
                this.props.setStateEdit(true);
          }else{
                this.setState({nameInput: this.props.edit.naem, nameDanger:"Please, change the field. Value must be a string"})
         }
     };
     changeInputModel = (EO) =>{
                this.props.setStateEdit(false);
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({modelName: EO.target.value , modelDanger:"OK"})
                this.props.setStateEdit(true);
          }else{
                this.setState({modelName: this.props.edit.model, modelDanger:"Please, change the field. Value must be a string"})
         }
     };
     changeInputCost = (EO) =>{
          this.props.setStateEdit(false);
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({costInput : EO.target.value , costDanger:"OK"})
                this.props.setStateEdit(true);
          }else{
                this.setState({costInput: this.props.edit.cost, costDanger:"Please, change the field. Value must be a number"})
         }
     };
     changeInputCount = (EO) =>{
          this.props.setStateEdit(false);
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({countInput : EO.target.value , countDanger:"OK"})
                this.props.setStateEdit(true);
          }else{
                this.setState({countInput: this.props.edit.count, countDanger:"Please, change the field. Value must be a number"})
         }
     };

     funEditData = (id, img) => {
          if(this.props.PropforApp.statChange){
          const {PropforApp} = this.props;
          let editObject = PropforApp.items;
          editObject.map(item =>{
              if(item.id == id){ 
               if(this.state.nameInput != ""){item['name'] = this.state.nameInput}else{item['name'] = item.name}
               item['id'] = id
               item['img']= img
               if(this.state.modelName != ""){item['model'] = this.state.modelName}else{item['model'] = item.model}
               if(this.state.costInput != ""){item['cost'] = this.state.costInput}else{item['cost'] = item.cost}
               if(this.state.countInput != ""){item['count'] = this.state.countInput}else{item['count'] = item.count}
               };
               })
          this.props.setChangeItem(editObject);
          this.props.setEdit(0);
          }
     };

      cancelClicked = () => {
           console.log(this.props.PropforApp.statChange)
          if(this.props.PropforApp.statChange){
               this.props.setEdit(0)
               this.props.setStateEdit(true);
          }
     };

     render(){
          const {edit} = this.props;

   return (   <div className = {"NewProd"}>
                  <div className = {"header"}>
                     <h1>{"Edit a"+ " "+ edit.name}</h1>
                  </div>
                  <div className = {"storka"}>
                      <p>{"Id: "+ edit.id}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Name"}</p>
                      <input type='text' defaultValue =  {edit.name}  onChange = {this.changeInputName}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.nameDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Model"}</p>
                      <input type='text'  defaultValue = {edit.model} onChange = {this.changeInputModel}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.modelDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Cost"}</p>
                       <input type = "text"  defaultValue = {edit.cost} onChange = {this.changeInputCost}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.costDanger}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Count"}</p>
                      <input type = "text"  defaultValue = {edit.count} onChange = {this.changeInputCount}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.countDanger}</p>
                  </div>
                  <div className = {"stroka"}>
                       <Fragment>
                           <button className = "button" onClick = {() => this.funEditData(edit.id, edit.img)}>{"Save"}</button>
                      </Fragment>
                      <button className = "button" onClick = {() =>  this.cancelClicked()} style = {{marginLeft:"5px"}}>{"Cancel"}</button>
                  </div>
                </div>
     )       
               
              
          };
    };

    const mapStateToProps = store => {
     return{
       PropforApp: store.PropforApp,
       edit: store.edit,
     }
   }
   
   const mapDispatchToProps = dispatch => {
     return {
          setChangeItem: obj => dispatch(setChangeItem(obj)),
          setStateEdit : bool => dispatch(setStateEdit(bool)),
          setEdit: id => dispatch(setEdit(id))
     }
   }

   Edit.propTypes = {
     
     setChangeItem: PropTypes.func,   
     setStateEdit: PropTypes.func,   
     setEdit: PropTypes.func
   }

   export default connect(
     mapStateToProps,
     mapDispatchToProps
     )(Edit);