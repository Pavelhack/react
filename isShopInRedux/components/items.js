import React from 'react';
import PropTypes, { bool } from "prop-types"
import './index.css';
import { connect } from 'react-redux';
import { setChangeItem, setEdit, setData } from './action/PageActons.js';

class Item extends React.Component{

   funViewItem = (id,) => {
         this.state.funViewItem(id,)
   }


   funDelete = (arg) => {
            let arrlenght; 
            if(confirm("Delete?")){
             arrlenght = this.props.PropforApp.items.filter(item => item.id != arg)
               this.props.setChangeItem(arrlenght)
            }
      }
   

   setEditFun = (arg,arg1,arg2,arg3,arg4,arg5) =>{
    if (this.props.PropforApp.statChange){
      const id = arg, name = arg1, cost = arg3, model = arg2, count = arg4, img = arg5;
       this.props.setEdit(id,name,model,cost, count, img)
    };
 };

   onBtnClick = (arg,arg1,arg2,arg3,arg4) =>{
      const id = arg, name = arg1, cost = arg3, model = arg2, count = arg4;
      this.props.setData(id,name,model,cost, count)
 }

 stopfun(EO){
   EO.stopPropagation();
 }

   render(){ 
      const {PropforApp, data} = this.props 
      let ok = [],classView ,keyItem, nameItem, imgItem, modelItem, costItem, countItem, key;
      ok = PropforApp.items.map(i => {
                                if(i.id == data.id){
                                      classView = "orange"
                                    }else{
                                      classView = "Item"
                                    }
                                key = i.id,    
                                keyItem = i.id,
                                nameItem = i.name, 
                                imgItem = i.img, 
                                modelItem = i.model,
                                costItem = i.cost,
                                countItem = i.count
        return          <div key = {key} className = {classView} onClick ={() =>this.onBtnClick( i.id,i.name,i.model,i.cost,i.count) } >
                            <div className = {"divNameItem"}>
                                <p>{nameItem}</p>
                                <p>{keyItem}</p>
                                <p>{classView}</p>
                            </div>
                            <div className = {"photo"}>
                              <img className = {"img"} src = {imgItem}/> 
                            </div>
                            <div className = {"dataItem"}>      
                              <div className = {"divModelItem"}>    
                                          <p>{modelItem}</p>
                              </div>
                              <div className = {"divCost"}>     
                                          <p>{costItem + "$"}</p>
                              </div>
                              <div className = {"divCount"} onClick = {this.stopfun} >
                                          <button onClick = {() => this.setEditFun(i.id,i.name,i.model,i.cost,i.count,i.img )  }>{"Edit"}</button>
                                          <button onClick = { () =>  this.funDelete(i.id) }>{"delete"}</button>
                                          <p>{countItem}</p>
                              </div>
                            </div>    
                        </div>
                  })  
                  return   <div className = "ItemDiv">{ok}</div>
    }
  };

 const mapStateToProps = store => {
      return{
        PropforApp: store.PropforApp,
        data: store.data,
      }
    }
    
    const mapDispatchToProps = dispatch => {
      return {
            setChangeItem: obj => dispatch(setChangeItem(obj)),
            setData: (id,name,model,cost,count) => dispatch(setData(id,name, model, cost,count)),
            setEdit: (id, name, model, cost, count, img) => dispatch(setEdit(id,name, model, cost, count, img))
      }
    }

    Item.propTypes = {
      setChangeItem: PropTypes.func,
      viewID: PropTypes.number,
      setData: PropTypes.func,
      setEdit: PropTypes.func
    }
    
    export default connect(
      mapStateToProps,
      mapDispatchToProps
      )(Item);