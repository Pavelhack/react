const initialState = {
    name: 'Store React',
    items: require("D:/сайты/JS/React/react/isShopInRedux/array.json"),
    flag : false,
    statChange : true,
  }
  
  export function forAppReducer(state = initialState, action) {
    switch (action.type){
    case "SET_StatEdit":
      return {...state, statChange: action.payload};
    case "SET_ChangeItem":
        return{...state, items: action.payload};
    case "SET_Flag":
      return {...state, flag: action.payload};    
    default:
    return state
    }
  }