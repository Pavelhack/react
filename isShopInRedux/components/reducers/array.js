const initialState = {
    id: 0,
    name: "",
    model: "",
    cost: 0,
    count:0,
    img: "#",
  }
  
  export function editReducer(state = initialState, action) {
    switch (action.type){
    case "SET_EDIT":
      return {...state, id: action.payloadKey,
                        name: action.payloadName,
                        model: action.payloadModel,
                        cost: action.payloadCost,
                        count: action.payloadCount,
                        img: action.payloadImg,
              }
    default:
    return state
    }
  }

  export function dataReducer(state = initialState, action) {
    switch (action.type){
    case "SET_DATA":
      return {...state,
                       id: action.payloadKey,
                       name: action.payloadName,
                       model: action.payloadModel,
                       cost: action.payloadCost,
                       count: action.payloadCount
             }
    default:
    return state
    }
  }
