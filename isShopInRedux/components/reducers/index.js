import { combineReducers } from "redux";
import { editReducer } from './array.js';
import { dataReducer } from './array.js';
import { forAppReducer } from './forApp.js';

export const rootReducer = combineReducers( { 
    PropforApp: forAppReducer,
    edit: editReducer,
    data: dataReducer,     
   })

