
export function setEdit(key, name, model, cost, count ,img) {
    return {
      type: 'SET_EDIT',
        payloadImg: img,
        payloadKey: key,
        payloadName: name,
        payloadModel: model,
        payloadCost: cost,
        payloadCount: count
    }
  }

export function setFlag(bool){
  return{
    type: "SET_Flag",
    payload: bool,
  }
}
export function setStateEdit(bool){
  return{
    type: "SET_StatEdit",
    payload: bool,
  }
}

export function setChangeItem(obj){
  return{
    type: "SET_ChangeItem",
    payload: obj,
  }
}

export function setData(key,name,model,cost,count){
      return{
        type: "SET_DATA",
        payloadKey: key,
        payloadName: name,
        payloadModel: model,
        payloadCost: cost,
        payloadCount: count
      }
}