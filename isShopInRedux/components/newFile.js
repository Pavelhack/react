import React, { Fragment } from 'react';
import PropTypes, { checkPropTypes } from "prop-types";
import './index.css';
import { connect } from 'react-redux';
import { setFlag , setChangeItem} from './action/PageActons.js';


class Newfile extends React.Component{
  
     state ={
        id : Math.round(1000*Math.random()),
        nameInput : "Exemple : phone",
        nameDanger :  "Please, fill the field. Value must be a string",
        costInput : "Exemple : 32",
        costDanger : "Please, fill the field. Value must be a number",
        countInput : "Exemple : 3",
        countDanger : "Please, fill the field. Value must be a number",
        modelName : "Exemple : Samsung",
        modelDanger : "Please, fill the field. Value must be a string",
     };

     changeInputName = (EO) =>{
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({nameInput : EO.target.value , nameDanger:"OK"})
          }else{
                this.setState({nameDanger:"Please, fill the field. Value must be a string"})
         }
     };
     changeInputModel = (EO) =>{
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({modelName: EO.target.value , modelDanger:"OK"})
          }else{
                this.setState({modelDanger:"Please, fill the field. Value must be a string"})
         }
     };
     changeInputCost = (EO) =>{
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({costInput : EO.target.value , costDanger:"OK"})
          }else{
                this.setState({costDanger:"Please, fill the field. Value must be a number"})
         }
     };
     changeInputCount = (EO) =>{
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({countInput : EO.target.value , countDanger:"OK"})
          }else{
                this.setState({countDanger:"Please, fill the field. Value must be a number"})
         }
     };
     cancelClicked = () => {
      this.props.setFlag(false);
     };
/// there is a new obj from newFile.js that will be to add in MAIN array with items
     funAddData = () => {
          let check = this.state;
          let newObj, objItems, obj = [check.nameInput,check.costInput,check.countInput,check.modelName]
               class User {
                         constructor(obj){
                              this.id = Math.round(1000*Math.random()),
                              this.name = obj[0],
                              this.cost = obj[1],
                              this.count = obj[2],
                              this.img = "./images/no_logo.png",
                              this.model = obj[3]
                         }
                    }
                    if((check.nameDanger && check.modelDanger && check.costDanger && check.countDanger) == "OK"){ 
               newObj =  new User(obj)
               objItems = this.props.PropforApp.items;
               objItems.push(newObj)
               this.props.setChangeItem(objItems);
               this.props.setFlag(false);
                    }
     };

     render(){
          const {} = this.props
       return( 
                <div className = {"NewProd"}>
                  <div className = {"header"}>
                     <h1>{"Add new product"+ "  "}</h1>
                  </div>
                  <div className = {"storka"}>
                      <p>{"Id: "+ this.state.id}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Name"}</p>
                      <input type='text' defaultValue = {this.state.nameInput} onChange = {this.changeInputName}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.nameDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Model"}</p>
                      <input type='text' defaultValue = {this.state.modelName} onChange = {this.changeInputModel}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.modelDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Cost"}</p>
                       <input type = "text" defaultValue = {this.state.costInput} onChange = {this.changeInputCost}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.costDanger}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Count"}</p>
                      <input type = "text" defaultValue = {this.state.countInput} onChange = {this.changeInputCount}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.countDanger}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Image"}</p>
                      <input type = "file"></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{"Please, choice the field."}</p>
                  </div>
                  <div className = {"stroka"}>
                       <Fragment>
                           <button className = "button" onClick = {() => this.funAddData()}>{"Save"}</button>
                      </Fragment>
                      <button className = "button" onClick = {() => this.cancelClicked()} style = {{marginLeft:"5px"}}>{"Cancel"}</button>
                  </div>
                </div>
       )
     };
    };

    Newfile.propTypes = {
   }

   const mapStateToProps = store => {
     return{
       PropforApp: store.PropforApp,
       data: store.data,
       edit: store.edit,
     }
   }
   
   const mapDispatchToProps = dispatch => {
     return {
       setFlag: bool => dispatch(setFlag(bool)),
       setChangeItem: obj => dispatch(setChangeItem(obj))
     }
   }
   
   Newfile.propTypes = {
     setFlag: PropTypes.func,
     setChangeItem : PropTypes.func
   }
   
   export default connect(
     mapStateToProps,
     mapDispatchToProps
     )(Newfile);
    //this.state.nameInput,this.state.costInput,this.state.countInput   