import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import { connect } from 'react-redux';
import './index.css';

class ViewItem extends React.Component{

    static propTypes = { 
        infoDelete : PropTypes.bool,
        id : PropTypes.number,
        name : PropTypes.string,
        model : PropTypes.string,
        cost : PropTypes.number,
        count : PropTypes.number
      };
  
     state ={
     };

 render(){
    const {data } = this.props 
     if(data.id == 0){
         return null
 }else{ 
     return(
                <div className = "NewProd">
                            <div className = {"header"}>
                                <h1>{"Information about"+"  "+ data.name}</h1>
                            </div>
                                <h3>{"ID:"+ data.id}</h3>
                                <h3>{"Model :"+" "+ data.model}</h3>
                                <h3>{"Cost :"+" "+ data.cost+"$"}</h3>
                                <h3>{"Count :"+" "+ data.count}</h3>
                </div>
           )
        }
    }
}
const mapStateToProps = store => {
    return{
      array: store.array,
      data: store.data,
    }
  }
  
  export default connect(
    mapStateToProps,
    )(ViewItem);
