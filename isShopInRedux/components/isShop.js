import React, { Fragment } from 'react';
import PropTypes from "prop-types";
import './index.css';
import Item  from './items.js'; 
import Edit from './edit.js';
import ViewItem from './viewItem.js';
import Newfile from "./newFile.js";
import { connect } from 'react-redux';
import { setFlag} from './action/PageActons.js';
class IsShop extends React.Component{

  //there is func for show a newFile (if flag true) 
  funNewProd = (arg) =>{
    this.props.setFlag(arg)
  };

    render() {
      const {PropforApp, data, edit,} = this.props
             if (PropforApp.flag){ 
    return(  <Fragment>
            <div className ={"IsShopFrame"}>
              <div className = {"header"}>
                   <h1>{PropforApp.name}</h1>
              </div>
                  <Item/> 
            </div>
                  <Newfile />
              </Fragment>
            )
    } else if(edit.id == 0){
     
    return(  
             <Fragment>  
              <div className ={"IsShopFrame"}>
                <div className = {"header"}>
                    <h1>{PropforApp.name}</h1>
                </div>
                    <Item />
                    <button className = "buttonNew" onClick = {() => this.funNewProd(true)}>{"New Product"}</button>
              </div>
              <div>
                    <ViewItem key = {data.id}/>
              </div>
            </Fragment>
             )
          }else{ 
    return( 
             <Fragment>  
              <div className ={"IsShopFrame"}>
                <div className = {"header"}>
                    <h1>{this.props.title}</h1>
                </div>
                    <Item />
                </div>
                <div>
                < Edit key = {edit.id} />
                </div>
            </Fragment>
          )
         };
    };
};

const mapStateToProps = store => {
  return{
    PropforApp: store.PropforApp,
    data: store.data,
    edit: store.edit,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setFlag: bool => dispatch(setFlag(bool)),
  }
}

IsShop.propTypes = {
  setFlag: PropTypes.func,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(IsShop);