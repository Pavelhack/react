import React  from 'react';
import ReactDOM from 'react-dom';
import './components/index.css';
import { Provider } from 'react-redux';
import {store} from './components/configureStore.js'
import App from './components/App.js'

ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>, document.getElementById('container') 
  );