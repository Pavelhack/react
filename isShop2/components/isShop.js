import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import './index.css';
import Item  from './items.js'; 
import Edit from './edit.js';
import ViewItem from './viewItem.js';
import Newfile from "./newFile.js";    
class IsShop extends React.Component{

  static propTypes  = {
    title : PropTypes.string,
    flag: PropTypes.bool,
    item : PropTypes.arrayOf(
       PropTypes.shape({
          count: PropTypes.number.isRequired,
          name: PropTypes.string.isRequired,
          cost: PropTypes.number.isRequired,
          img: PropTypes.any,
    })
    ),
  };

  static defaultProps = { 
    nathing: "Here is empty" 
  };

  state = {
    arr: this.props.item,
    flag: this.props.flag,
    idEdit : 0,
    viewId : 0,
    statChange : false,
   
  };

  FunEdit = (arg) => {
     this.setState({idEdit : arg})
  };

  funNewProd = (arg) =>{
    if(arg){
    this.setState({flag : true})
    }else{
    this.setState({flag : false})
    }
  };

  funViewItem = (id) =>{
    this.setState({viewId : id})
  };

  FunInfoDel = (infoDelete) =>{
     this.setState({arr : infoDelete})
  }
  
  /// this is a new obj from newFile.js that will be to add in array in this.state.arr
  FunNewObj = (obj) => {
    let array = [];
    let object = this.props.item;
    object.push(obj)
    object.map(item =>{
          item['id'] = Math.round(1000*Math.random()), item['name'] = item.name, item['img']= item.img, item['model'] = item.model,
          item['cost'] = item.cost, item['count'] = item.count
      array.push(item)
     });
    this.setState({arr : array})
  };

  FunStateChange = (arg) => {
    this.setState({statChange : arg})
  }

    render() {
             if (this.state.flag){ 
    return(  <Fragment>
            <div className ={"IsShopFrame"}>
              <div className = {"header"}>
                   <h1>{this.props.title}</h1>
              </div>
                  <Item prop = {this.state.arr} condflag = {this.state.flag} /> 
            </div>
                  <Newfile cbfunProd = {this.funNewProd} сbFunNewObj = {this.FunNewObj} />
              </Fragment>
            )
    } else if(this.state.idEdit == 0){
    return(  
             <Fragment>  
              <div className ={"IsShopFrame"}>
                <div className = {"header"}>
                    <h1>{this.props.title}</h1>
                </div>
                    <Item prop = {this.state.arr}  cbFunEdit = {this.FunEdit} cbfunViewItem = {this.funViewItem} cbFunInfoDel = {this.FunInfoDel} statChange = {this.state.statChange}/>
                    <button className = "buttonNew" onClick = {() => this.funNewProd(true)}>{"New Product"}</button>
              </div>
              <div>
                    <ViewItem prop = {this.state.arr} viewId = {this.state.viewId}/>
              </div>
            </Fragment>
             )
          }else{ 
    return( 
             <Fragment>  
              <div className ={"IsShopFrame"}>
                <div className = {"header"}>
                    <h1>{this.props.title}</h1>
                </div>
                    <Item prop = {this.state.arr}  statChange = {this.state.statChange}  />
                </div>
                <div>
                < Edit key = {this.state.idEdit} prop = {this.state.arr} сbFunInfoDel = {this.FunInfoDel} cbFunEdit = {this.FunEdit} cbStatChange = {this.FunStateChange} itemId = {this.state.idEdit} />
                </div>
            </Fragment>
          )
         };
    };
};

export default IsShop;