import React, { Fragment } from 'react';
import PropTypes, { bool } from "prop-types"
import './index.css';

class Edit extends React.Component{

    static propTypes = { 
     itemId : PropTypes.number.isRequired,
     prop : PropTypes.array,
     cbFunEdit : PropTypes.func,
     сbFunInfoDel : PropTypes.func,
     cbStatChange : PropTypes.func,
      };
  
     state ={
       itemId : this.props.itemId,
       nameInput : "",
       nameDanger :  "Please, change the field. Value must be a string",
       costInput : "",
       costDanger : "Please, change the field. Value must be a number",
       countInput : "",
       countDanger : "Please, change the field. Value must be a number",
       modelName : "",
       modelDanger : "Please, change the field. Value must be a string",
       statChange : this.props.cbStatChange,
     };

     changeInputName = (EO) =>{
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({nameInput : EO.target.value , nameDanger:"OK"})
                this.state.statChange(true);
          }else{
                this.setState({nameDanger:"Please, change the field. Value must be a string"})
         }
     };
     changeInputModel = (EO) =>{
          if(EO.target.value!= "" &&  isNaN(+EO.target.value)){ 
                this.setState({modelName: EO.target.value , modelDanger:"OK"})
                this.state.statChange(true);
          }else{
                this.setState({modelDanger:"Please, change the field. Value must be a string"})
         }
     };
     changeInputCost = (EO) =>{
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({costInput : EO.target.value , costDanger:"OK"})
                this.state.statChange(true);
          }else{
                this.setState({costDanger:"Please, change the field. Value must be a number"})
         }
     };
     changeInputCount = (EO) =>{
         
          if(EO.target.value!= "" &&  !isNaN(+EO.target.value)){ 
                this.setState({countInput : EO.target.value , countDanger:"OK"})
                this.state.statChange(true);
          }else{
                this.setState({countDanger:"Please, change the field. Value must be a number"})
         }
     };

     funEditData = (id, img) => {
          let editObject = this.props.prop
          editObject.map(item =>{
               if(item.id == id){ 
                  if(this.state.nameInput != ""){item['name'] = this.state.nameInput}else{item['name'] = item.name}
                  item['id'] = id
                  item['img']= img
                  if(this.state.modelName != ""){item['model'] = this.state.modelName}else{item['model'] = item.model}
                  if(this.state.costInput != ""){item['cost'] = this.state.costInput}else{item['cost'] = item.cost}
                  if(this.state.countInput != ""){item['count'] = this.state.countInput}else{item['count'] = item.count}
                };
               })
               console.log(editObject)
               this.props.сbFunInfoDel(editObject);
               this.props.cbFunEdit(0);
               this.state.statChange(false);
     };

      cancelClicked = () => {
      this.props.cbFunEdit(0);
      this.state.statChange(false);
     };

     render(){
          let keyItem, nameItem, modelItem, imgItem, costItem, countItem, fun, ok = [];
          this.props.prop.map(item =>{
             keyItem = item.id,  nameItem = item.name, imgItem = item.img,
             modelItem = item.model, costItem = item.cost, countItem = item.count
                 if(this.props.itemId == item.id){
       fun =   <div className = {"NewProd"}>
                  <div className = {"header"}>
                     <h1>{"Edit a"+ " "+ nameItem}</h1>
                  </div>
                  <div className = {"storka"}>
                      <p>{"Id: "+ keyItem}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Name"}</p>
                      <input type='text' defaultValue =  {nameItem}  onChange = {this.changeInputName}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.nameDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Model"}</p>
                      <input type='text'  defaultValue = {modelItem} onChange = {this.changeInputModel}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.modelDanger}</p>
                  </div>

                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Cost"}</p>
                       <input type = "text"  defaultValue = {costItem} onChange = {this.changeInputCost}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.costDanger}</p>
                  </div>
                  <div className = {"stroka"} >
                       <p style = {{margin : "5px"}}>{"Count"}</p>
                      <input type = "text"  defaultValue = {countItem} onChange = {this.changeInputCount}></input>
                       <p className = "inform" style = {{color: 'red', margin: "5px" }} >{this.state.countDanger}</p>
                  </div>
                  <div className = {"stroka"}>
                       <Fragment>
                           <button className = "button" onClick = {() => this.funEditData(item.id, item.img)}>{"Save"}</button>
                      </Fragment>
                      <button className = "button" onClick = {() =>  this.cancelClicked()} style = {{marginLeft:"5px"}}>{"Cancel"}</button>
                  </div>
                </div>
                 }
               });
               return fun
          };
    };
    export default  Edit;