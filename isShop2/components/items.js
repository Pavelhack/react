import React from 'react';
import PropTypes, { bool } from "prop-types"
import './index.css';
class Item extends React.Component{

   static propTypes = { 
      prop : PropTypes.array,
      condflag : PropTypes.bool,
      cbFunEdit : PropTypes.func,
      cbfunViewItem : PropTypes.func,
      cbFunInfoDel : PropTypes.func,
      statChange : bool,
    };

   state ={
      id : 0,
      cbFunEdit : this.props.cbFunEdit,
      arr: this.props.prop,
      condflag: this.props.condflag,
      funViewItem : this.props.cbfunViewItem,
      cbFunInfoDel : this.props.cbFunInfoDel,
   };
  
   funHighlight = (id) => {
      if (this.props.condflag == undefined && !this.props.statChange){
            this.setState({id : id}) 
      }
   };

   funViewItem = (id,) => {
         this.state.funViewItem(id,)
   }

   funEdit = (arg) =>{
      if (this.props.condflag == undefined && !this.props.statChange){
            this.state.cbFunEdit(arg)      
      };
   };

   funDelete = (arg) => {
            let arrlenght = this.state.arr;
         if (this.props.condflag == undefined && !this.props.statChange){ 
            if(prompt("bla bla bla") != null){
            arrlenght.splice((arrlenght.map(item => item.id == arg).indexOf(true)),1)
               this.setState({arr : arrlenght})
               this.state.cbFunInfoDel(arrlenght)
            }
      }
   };

   

   render(){
      let ok = [],keyItem, nameItem, imgItem, modelItem, costItem, countItem, fun;
      this.state.arr.map(item =>{
               keyItem = item.id,  nameItem = item.name, imgItem = item.img,
               modelItem = item.model, costItem = item.cost, countItem = item.count
               if(this.state.id == keyItem){ 
                   fun =<div key = {keyItem} className = {"orange"} >
                          <div className = {"divNameItem"}>
                                <p>{nameItem}</p>
                                <p>{keyItem}</p>
                          </div>
                          <div className = {"photo"}>
                                <img className = {"img"} src = {imgItem}/> 
                          </div>
                            <div className = {"dataItem"}>      
                            <div className = {"divModelItem"}>    
                                      <p>{modelItem}</p>
                                  </div>
                                  <div className = {"divCost"}>     
                                     <p>{costItem + "$"}</p>
                                  </div>
                                  <div className = {"divCount"}>
                                        <button onClick = { () => this.funEdit(item.id)}>{"Edit"}</button>
                                        <button onClick = { () =>  this.funDelete(item.id) }>{"delete"}</button>
                                        <p>{countItem}</p>
                                  </div>
                          </div>    
                        </div>
                        ok.push(fun)
                      }else{ 
                        fun = <div key = {keyItem} className = {"Item"} onClick ={() => {this.funHighlight(item.id);this.funViewItem(item.id);}}>
                        <div className = {"divNameItem"}>
                              <p>{nameItem}</p> 
                              <p>{keyItem}</p>
                        </div>
                        <div className = {"photo"}>
                              <img className = {"img"} src = {imgItem}/> 
                        </div>
                          <div className = {"dataItem"}>      
                          <div className = {"divModelItem"}>    
                                    <p>{modelItem}</p>
                                </div>
                                <div className = {"divCost"}>     
                                    <p>{costItem + "$"}</p>
                                </div>
                                <div className = {"divCount"}>
                                      <button onClick = { () => this.funEdit(item.id)}>{"Edit"}</button>
                                      <button onClick = { () => this.funDelete(item.id) }>{"delete"}</button>
                                      <p>{countItem}</p>
                                </div>
                        </div>    
                      </div>
                     
                      ok.push(fun)
                  }

                    })
                  return   <div className = "ItemDiv">{ok}</div>
    }
 };

 export default  Item;