"use strict";

import React  from 'react';
import ReactDOM from 'react-dom';
import './components/index.css';
import IsShop from './components/isShop.js';
import { Provider } from 'react-redux';
import {createStore} from "redux";
//import configureStore from './store/configureStore';

const reducer = (state = 0, action) =>{
  switch (action.type){
    case "inc":
      return state +1;
    case 'dec':
      return state -1;
    default: return state;
  }
}
const store = createStore(reducer)

const update = () => {
  console.log(store.getState());
}

store.subscribe(update);

let flag = false;
let nameStor = "Store React";
let itemArr = require("./array.json");

    ReactDOM.render(
      <Provider store={store}>
        <IsShop
          title = {nameStor}
          item = {itemArr}
          flag = {flag}
        />
        </Provider>
        
     , document.getElementById('container') 
    );
