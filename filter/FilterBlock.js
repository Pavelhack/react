let FilterBlock = React.createClass({

  displayName: 'FilterBlock',

  propTypes: {
    fruits: React.PropTypes.arrayOf(
      React.PropTypes.shape({
        code: React.PropTypes.number,
        name: React.PropTypes.string.isRequired,
      })
    )
  },

  getInitialState: function () {
    return {
      flag: false,
      text: "",
    }
  },

  valueText: function (EO) {
    this.setState({
      text: EO.target.value
    })
  },

  getFruitsSort: function (EO) {
    this.setState({
      flag: EO.target.checked
    })
  },

  workFunction: function () {
    let val = this.state.text;
    let sorter =  this.props.fruits.map(item => item.name).sort();
    let sorterText = [];
    if (this.state.flag == true && this.state.text == "") {
           // sorter = this.props.fruits.map(item => item.name).sort()
       return { sort: sorter
       }
       
    } else if (this.state.flag == false && this.state.text != ""){
              sorterText = this.props.fruits.map(function (item) {
                if(item.name.includes(val))
                  return item.name
                 })
        return {sort:sorterText 
                    }  

    }else if (this.state.flag == true && this.state.text != ""){ 
             sorterText = sorter.map(item => {
              if(item.includes(val))
                return item
               })
               return {sort: sorterText}
        }
    else{
          return{sort: this.props.fruits.map(item => item.name)}
        }
  },

  buttonClear: function(EO){
     if(EO.target.click){
        this.valueText.text = "", 
        this.setState(
        {flag: false, text: ""}
      )
    }
  },


  render: function () {
    let arrFrut = [];
    this.workFunction().sort.map(name => {
      arr = React.DOM.p(null, name)
      arrFrut.push(arr)
    })

    return React.DOM.div({
        className: 'FilterBlock'
      },
      React.DOM.div({
          className: "DivInputRadio",
          onClick: this.getFruitsSort,
          
        },
        React.DOM.input({
          type: 'checkbox',
          checked: this.state.flag,
        }),
      ),
      React.DOM.div({
          className: "BlockText"
        },
        React.DOM.input({
          type: 'text',
          defaultValue: "",
          onChange: this.valueText,
          value: this.state.text,
        })
      ),
      React.DOM.div({
          className: "BottonBlock"
        },
        React.DOM.button({
          onClick: this.buttonClear
        }, "clear")
      ),
      React.DOM.div({
          className: "DivFieldBlock"
        },
        arrFrut
      )
    )
  }
})